import './App.css';
import React, { useState, useEffect } from 'react';
import WallPaperComponent from './components/landing/Wallpaper-Component';
import DisplayComponent from './components/landing/Display-Component';
import { Container } from 'react-bootstrap';
// import DotsScrollBar from './components/common/dots-scroll-bar';
import FooterComponent from './components/common/Footer-Component';
import NavbarComponent from './components/common/Navbar-Component';
import SideBarComponent from './components/common/SideBar-Component';
import { apiUtils, getCats, getTags } from './api/apiUtils';

function App() {
  const [posts, setPosts] = useState([]);
  const [display, setDisplay] = useState(false);
  const [categories, setCategories] = useState([]);
  const [tags, setTags] = useState([]);

  function CloseSidebar() {
    if (display === false) {
      setDisplay(true);
    }
    console.log(display);
  }

  function OpenDisplay() {
    if (display === true) setDisplay(false);
  }

  useEffect(() => {
    apiUtils()
      .then((results) => {
        setPosts(results);
      })
      .catch((err) => {
        throw err;
      });
  }, []);
  useEffect(() => {
    getCats()
      .then((results) => setCategories(results))
      .catch((error) => {
        throw error;
      });
  }, []);
  useEffect(() => {
    getTags()
      .then((results) => setTags(results))
      .catch((error) => {
        throw error;
      });
  }, []);
  function LoadPosts(start, end) {
    apiUtils()
      .then((r) => setPosts(r))
      .catch((e) => {
        throw e;
      });
  }
  return (
    <Container fluid className="App p-0">
      {/* <DotsScrollBar /> */}
      <NavbarComponent openDis={OpenDisplay} />
      <SideBarComponent display={display} cats={categories} closeDis={CloseSidebar} tags={tags} />
      <WallPaperComponent />
      <DisplayComponent
        posts={posts}
        start={posts.indexOf(0)}
        end={posts.indexOf(posts.length)}
        loadpostsfunc={LoadPosts}
      />
      <FooterComponent />
    </Container>
  );
}

export default App;
