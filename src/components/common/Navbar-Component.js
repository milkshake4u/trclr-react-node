import React from 'react';
import '../../component-stylesheets/navbar-component.css';
import { Navbar, Button } from 'react-bootstrap';

const NavbarComponent = (props) => {
  return (
    <>
      <Navbar className="navHome px-0 text-capitalize">
        <Button
          onClick={() => props.openDis()}
          className="btn btn-outline-secondary ml-3 mr-3 bg-white"
        >
          <img src="/images/icons8-menu.svg" alt="hamburger_icon_menu" id="hamburgur-icon" />
        </Button>
        <Navbar.Brand href="/">
          <img
            src="/images/icons8-phone.svg"
            width="30"
            height="30"
            className="d-inline-block align-top rounded-circle"
            alt="Truecaller_logo"
          />
          <p className="ps-3">Truecaller blog</p>
        </Navbar.Brand>
      </Navbar>
    </>
  );
};

export default NavbarComponent;
