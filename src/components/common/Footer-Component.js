import React from 'react';
import '../../component-stylesheets/footer-component.css';
import { Container, Row, Col } from 'react-bootstrap';

const FooterComponent = () => {
  return (
    <>
      <footer className="site-footer">
        <Container>
          <Row>
            <Col sm={12} md={12}>
              <h6>About</h6>
              <p className="text-justify abouttext">
                Truecaller is a smartphone application that has features of caller-identification,
                call-blocking, flash-messaging, call-recording (on Android up to version 8), Chat &
                Voice by using the internet. It requires users to provide a standard cellular mobile
                number for registering with the service. The app is available for Android and iOS.
              </p>
            </Col>

            <Col xs={6} md={6} className="p-0">
              <h6>Categories</h6>
              <ul className="footer-links">
                <li>
                  <a href="#about">About Us</a>
                </li>
                <li>
                  <a href="#blog">Truecaller Blog</a>
                </li>
                <li>
                  <a href="#career">Careers </a>
                </li>
                <li>
                  <a href="#search">Trecaller search</a>
                </li>
              </ul>
            </Col>

            <Col xs={6} md={6} className="p-0">
              <h6>Quick Links</h6>
              <ul className="footer-links">
                <li>
                  <a href="#contact_us">Contact Us</a>
                </li>
                <li>
                  <a href="#terms">Terms Of Service</a>
                </li>
                <li>
                  <a href="#poc">Privacy Policy</a>
                </li>
                <li>
                  <a href="#sitemap">Sitemap</a>
                </li>
              </ul>
            </Col>
          </Row>
        </Container>
        <Container>
          <Row>
            <Col md={12} sm={6} xs={12}>
              <p className="copyright-text">Copyright &copy; 2021</p>
            </Col>

            <Col md={12} sm={6} xs={12} className="ml-auto">
              <ul className="social-icons">
                <li>
                  <a className="facebook" href="https://facebook.com">
                    <img src="/images/icons8-facebook.svg" alt="facebook-link" />
                  </a>
                </li>
                <li>
                  <a className="twitter" href="https://youtube.com">
                    <img src="/images/icons8-play-button.svg" alt="youtube-link" />
                  </a>
                </li>
                <li>
                  <a className="dribbble" href="https://linkedin.com">
                    <img src="/images/icons8-linkedin.svg" alt="linkedin_link" />
                  </a>
                </li>
                <li>
                  <a className="linkedin" href="https://twitter.com">
                    <img src="/images/icons8-twitter.svg" alt="twitter_link" />
                  </a>
                </li>
              </ul>
            </Col>
          </Row>
        </Container>
      </footer>
    </>
  );
};

export default FooterComponent;
