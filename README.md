## LIVE ACCESS: [(click to access 🔗)](https://vibrant-blackwell-647bea.netlify.app/)

## LIVE ACCESS API-DOCS: [(click to access 🔗)](https://vibrant-blackwell-647bea.azurewebsites.net/apidocs/)

## INFRASTRUCTURE (HOST)

- Azure AppService [Backend Deployment]
- Netlify Service [Frontend deployment]

## LANGUAGES USED

- ReactJS
- NodeJS
- YAML
- JSON
- MarkDown

## TOOLS USED

- Docker
- Git
- ESLint
- Swagger Docs

### BRIEF UNDERSTANDING OF THE PROJECT:

> NODE BACKEND

The file Structure in Node Consists of 4-5 Categories

- src/models (Db related queries)
- src/controllers (Business logic and like 'operator')
- src/utils (all helper functions (ex- apiquery, etc.))
- src/routers (consists of all routes)
- swagger.json (swagger-docs)

Primary focus has been to increase code reusablity without too much complexity

> REACT FRONTEND

The file Structure in React Consists of 3-4 Categories

- src/api (api query to backend)
- src/Component-StyleSheet (component stylesheets)
- src/components
  - Common (common components like nav, footer)
  - Distinct (components like cover, mainSection, etc.)
- public/images (all assets like images, gifs, video, reside here)
- public/manifest.json (app manifest)
- src/fonts (custom fonts reside here)

> [FRONTEND CODE](https://bitbucket.org/srijeet_b/trclr-react-frontend/src/master/)

> [BACKEND CODE](https://bitbucket.org/srijeet_b/trclr-node-backend/src/master/)
